import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import Example from './components/VuexExample';
import EmptyRoute from './components/EmptyRoute';
import EntryPoint from './components/EntryPoint';
import Questions from './components/Questions';
import Final from './components/Final';

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    count: 0,
    data: null,
  },
  mutations: {
    increment (state) {
      state.count++
    },
    decrement (state) {
      state.count--
    },
    setData (state, payload) {
      state.data = payload;
    }
  }
});

const routes = [
  { path: '/', component: EntryPoint },
  { path: '/counter', component: Example },
  { path: '/empty', component: EmptyRoute },
  { path: '/questions', component: Questions },
  { path: '/final', component: Final },
];

const router = new VueRouter({
  routes
});


new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
